var MyApp=angular.module('myAppPlacar', ['ngRoute', 'ngResource']);
  MyApp.config(['$routeProvider','$httpProvider', function($routeProvider,$httpProvider){
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $routeProvider
      .when('/', { 
        templateUrl: 'templates/apresentacao.html',
      })
      .when('/participantes', { 
        templateUrl: 'templates/participantes.html',
      })
      .when('/jogo', { 
        templateUrl: 'templates/jogo.html',
      })
      .when('/historico', { 
        templateUrl: 'templates/historico.html',
      })
  }]);
  MyApp.controller('MainCtrl', ['$rootScope','$scope','$location',
    function ($rootScope,$scope,$location) {
      $scope.tabAtiva=0;
      $location.path("/");
      $scope.goPagina=function(tab){
        switch (tab) {
          case 1:
            $scope.tabAtiva=1;
            $location.path("/participantes");
            break;
          case 0:   
            $scope.tabAtiva=0;
            $location.path("/");
            break;
          case 2:
            $scope.tabAtiva=2;
            $location.path("/jogo");
            break;
          case 3:   
            $scope.tabAtiva=3;
            $location.path("/historico");
            break;
        }
    }
  }]);
  MyApp.controller('ctrlPartic', ['$rootScope','$scope','$location',
    function ($rootScope,$scope,$location) {
      $rootScope.participantes=JSON.parse(localStorage.getItem("participantes"));
      $scope.gravaPart=function(){
        var part=JSON.parse(localStorage.getItem("participantes"));
        if (part===null) part=[];
        part.push($scope.jogador);
        $scope.participantes=part;
        localStorage.setItem("participantes",JSON.stringify($scope.participantes));
        $('#myModal').modal('hide');
      }
  }]);

  

  
